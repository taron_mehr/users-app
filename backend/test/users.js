let { app } = require("../app");
let chai = require("chai");
let chaiHttp = require("chai-http");

chai.should();
chai.use(chaiHttp);

describe('User APIs', () => {
    it("It should return all users", (done) => {
        chai.request(app)
            .get("/api/users")
            .end((err, response) => {
                response.should.have.status(200);
                response.body.should.be.a('array');
                response.body.length.should.not.be.eq(0);
                done();
            });
    });
})