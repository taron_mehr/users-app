var express = require('express');
const { queryUsers } = require('../services/users.service');
var router = express.Router();

router.get('/', function(req, res, next) {
  const users = queryUsers();
  res.send(users);
});

module.exports = router;
