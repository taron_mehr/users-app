const serverless = require('serverless-http');
var express = require("express");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

app.use(logger("dev"));

app.use("/", indexRouter);
app.use("/api/users", usersRouter);

const port = process.env.PORT || "3000"

// Comment this line before deploying to AWS Lambda
app.listen(port, () => console.log(`Listening on: ${port}`));

module.exports = {
    app,
    handler: serverless(app)
};
