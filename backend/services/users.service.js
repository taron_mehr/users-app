const USERS_DATA = require('./users.json')

const queryUsers = () => {
    return USERS_DATA
};

module.exports = {
    queryUsers
}
