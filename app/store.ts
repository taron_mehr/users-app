import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import usersReducer from './users/state/reducer';

const rootReducer = combineReducers(
    {
        user: usersReducer,
    },
)

export type AppState = ReturnType<typeof rootReducer>

export default createStore(rootReducer, applyMiddleware(thunk));