import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import FaceList from './users/FaceList';
import store from './store';

export default function App() {
    return (
        <Provider store={store}>
            <View style={styles.container}>
                <FaceList />
                <StatusBar style="auto" />
            </View>
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
});
