import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Image, ActivityIndicator } from 'react-native';
import { fetchUsers } from './state/actions';
import { AppState } from '../store';

const Item = ({ name, avatar }: ItemProps) => (
    <View style={styles.item}>
        <Image
            style={styles.image}
            source={{ uri: avatar }}
        />
        <Text style={styles.name} numberOfLines={1} ellipsizeMode={"tail"}>{name}</Text>
    </View>
);

export default function FaceList() {
    const dispatch = useDispatch()
    const users = useSelector((state: AppState) => state.user.users)
    const isFetching = useSelector((state: AppState) => state.user.isFetching)
    useEffect(() => {
        dispatch(fetchUsers())
    }, [])

    const renderItem = ({ item }: any) => <Item name={item.name} avatar={item.avatar} />;

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={users}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
            {isFetching && <ActivityIndicator size={"large"}/>}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        justifyContent: 'center'
    },
    item: {
        backgroundColor: '#fff',
        flex: 1,
        padding: 10,
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#ececec',
    },
    name: {
        fontSize: 22,
        paddingLeft: 5,
        color: '#4f4f4f',
    },
    image: {
        width: 70,
        height: 70,
        backgroundColor: 'red',
    }
});

type ItemProps = {
    name: string;
    avatar: string;
}