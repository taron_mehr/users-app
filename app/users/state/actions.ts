import axios from 'axios';
import { User } from './types';
import { AnyAction } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { AppState } from '../../store';
import Constants from 'expo-constants'

export const FETCH_USERS_REQUEST = 'FETCH_USERS_REQUEST';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR';

export const API_URL = Constants.manifest.extra.api_url || "http://localhost:3000"

export type FetchUsersRequestAction = {
    type: typeof FETCH_USERS_REQUEST,
}

export type FetchUsersSuccessAction = {
    type: typeof FETCH_USERS_SUCCESS;
    users: User[];
}

export type FetchUsersErrorAction = {
    type: typeof FETCH_USERS_ERROR,
}

export type ConnectionActions =
    FetchUsersRequestAction |
    FetchUsersSuccessAction |
    FetchUsersErrorAction

export const fetchUsersRequest = (): ConnectionActions =>({
    type: FETCH_USERS_REQUEST
})

export const fetchUsersSuccess = (users: User[]): ConnectionActions =>({
    type: FETCH_USERS_SUCCESS,
    users
})

export const fetchUsersError = (): ConnectionActions =>({
    type: FETCH_USERS_ERROR,
})

export const fetchUsers = (): ThunkAction<any, AppState, any, AnyAction> => async (dispatch)  => {
    dispatch(fetchUsersRequest());
    try {
        const resp = await axios.get(`${API_URL}/api/users`)
        dispatch(fetchUsersSuccess(resp.data))
    } catch(e) {
        dispatch(fetchUsersError())
    }
};
