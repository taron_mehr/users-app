import { FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_ERROR, ConnectionActions } from './actions'
import { UserState } from './types'

const initialState: UserState = {
    users: [],
    isFetching: false,
    hasError: false,
}
export default function usersReducer (
    state = initialState,
    action: ConnectionActions
): UserState {
    switch (action.type) {
        case FETCH_USERS_REQUEST:
            return {
                ...state,
                isFetching: true,
                users: [],
                hasError: false,
            }
        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                users: action.users,
            }
        case FETCH_USERS_ERROR:
            return {
                ...state,
                isFetching: false,
                hasError: true,
            }
        default:
            return state;
    }
}
