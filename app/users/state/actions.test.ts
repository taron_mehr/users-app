import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from "axios-mock-adapter";
import axios from 'axios';
import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
  ConnectionActions,
  fetchUsersRequest,
  fetchUsersSuccess,
  fetchUsersError,
  API_URL,
  fetchUsers,
} from "./actions";

import { User } from "./types";

const mockStore = configureMockStore([thunk])
const mock = new MockAdapter(axios);


describe("actions", () => {
  it("should create an action for fetching users", () => {
    const expectedAction: ConnectionActions = {
      type: FETCH_USERS_REQUEST,
    };
    expect(fetchUsersRequest()).toEqual(expectedAction);
  });

  it("should create an action for fetching users success", () => {
    const users: User[] = [
      {
        id: "1",
        name: "Tom Bauer",
        avatar: "AVATAR_URL",
      },
    ];
    const expectedAction: ConnectionActions = {
      type: FETCH_USERS_SUCCESS,
      users,
    };
    expect(fetchUsersSuccess(users)).toEqual(expectedAction);
  });

  it("should create an action for fetching users error", () => {
    const expectedAction: ConnectionActions = {
      type: FETCH_USERS_ERROR,
    };
    expect(fetchUsersError()).toEqual(expectedAction);
  });

  it("should create an action for fetching users", () => {
    const users: User[] = [
      {
        id: "1",
        name: "Tom Bauer",
        avatar: "AVATAR_URL",
      },
    ];
    mock.onGet(`${API_URL}/api/users`).reply(200, users);
    const store = mockStore({ user: { users: []} });
    const expectedActions = [
      { type: FETCH_USERS_REQUEST },
      { type: FETCH_USERS_SUCCESS, users }
    ]
    return store.dispatch(fetchUsers() as any).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
});
