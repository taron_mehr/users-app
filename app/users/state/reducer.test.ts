import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR,
} from "./actions";
import usersReducer from "./reducer";
import { User, UserState } from "./types";

describe("usersReducer", () => {
  it("should return the initial state", () => {
    const initialState: UserState = {
      users: [],
      isFetching: false,
      hasError: false,
    }
    expect(
      usersReducer(undefined, { } as any)
    ).toEqual(initialState);
  });

  it("should handle FETCH_USERS_REQUEST", () => {
    const state = {
      isFetching: true,
      users: [],
      hasError: false,
    }
    const expectedState = {
      isFetching: true,
      users: [],
      hasError: false,
    }
    expect(
      usersReducer(state, { type: FETCH_USERS_REQUEST })
    ).toEqual(expectedState);
  });

  it("should handle FETCH_USERS_SUCCESS", () => {
    const users: User[] = [
      {
        id: "1",
        name: "Tom Bauer",
        avatar: "AVATAR_URL",
      },
    ];
    const state = {
      isFetching: true,
      users: [],
      hasError: false,
    }
    const expectedState = {
      isFetching: false,
      users: [...users],
      hasError: false,
    }

    expect(
      usersReducer(state, { type: FETCH_USERS_SUCCESS, users })
    ).toEqual(expectedState);
  });

  it("should handle FETCH_USERS_ERROR", () => {
    const state = {
      isFetching: true,
      users: [],
      hasError: false,
    }
    const expectedState = {
      isFetching: false,
      users: [],
      hasError: true,
    }
    expect(
      usersReducer(state, { type: FETCH_USERS_ERROR })
    ).toEqual(expectedState);
  });
});
