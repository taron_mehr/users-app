export type User = {
    id: string;
    name: string;
    avatar: string;
}
export type UserState = {
    users: User[];
    isFetching: boolean;
    hasError: boolean;
}