import React from "react";
import renderer from "react-test-renderer";
import configureMockStore from 'redux-mock-store';
import FaceList from "./FaceList";
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';

describe("FaceList", () => {
  let store: any;
  let mockStore: any;
  beforeEach(() => {
    jest.useFakeTimers();
    mockStore = configureMockStore([thunk])
    store = mockStore({ user: { users: []} });
  })
  it("renders correctly", () => {
    const tree = renderer.create(<Provider store={store}><FaceList /></Provider>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
