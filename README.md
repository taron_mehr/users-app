# User's app

Users app is a mobile app build with [React Native](https://reactnative.dev/) and managed by [Expo](https://expo.io/).

The backend is built with ExpressJS and for the showcase it is using [Serverless](https://www.serverless.com/) framework to deploy as AWS Lambda functions.

## Demo
You can download expo app from app store then open [this link](exp://exp.host/@go-native/app) to see demo app. Alternatively scan the following QR code:

![alt text](https://gitlab.com/taron_mehr/users-app/-/raw/master/qr.png "QR code")

Here is the screenshot.

![alt text](https://gitlab.com/taron_mehr/users-app/-/raw/master/screen.png "Users app screenshot")

## Development - Backend

Backend is build with ExpressJS.

### **Steps to run backend locally**
`cd backend` 

`npm i` - install dependencies.

`npm start` - listens to 3000 port by default.

`curl localhost:3000/api/users` -  lists users.

### **Deploy to AWS Lambda**

Assuming you already have AWS profile set in you machine(check `~/.aws/credentials` folder) install Serverless.

`npm install serverless -g`

`./serverless.yml` contains necessary instructions for configuring new API Gateway and Lambda handler.
Before deploying to AWS Lambda comment out the **line 18** in **app.js**(Serverless deployment doesn't need to listen to a port)

run ```serverless deploy```

After finishing the deployment successfully the url for API should be provided.
It looks similar to this "https://nfmlhzimy9.execute-api.eu-north-1.amazonaws.com/dev".

### **Testing**
**Mocha** test framework in combination with **chai** is used to test rest apis.

run `npm test`

## Development - mobile app

Mobile app is built with React Native managed by Expo.

### **Steps to run backend locally**

`npm i -g expo-cli` - installs ***expo-cli***.

`npm i` - installs dependencies.

>  Note: By default the app points to Lambda functions hosted in my AWS account. You can change the *app_url* in *app.json* file.

`npm start` - starts the app and provides a qr code to open it in your phone.

### **Deploy to Expo**

You can deploy the app to expo hosted environment to share it with others.

`expo publish`

### **Testing**

**Jest** test runner in combination with **react-test-renderer** is used for testing.

run `npm test`
